const Web3 = require('web3');
const Tx = require('ethereumjs-tx').Transaction;


const helpers = {};

// Smart contract initialization
const abi = require('../security/smartcontract/abi.json');

const infuraProyUrl = process.env.INFURA_PROJECT_URL;
const web3 = new Web3(new Web3.providers.HttpProvider(infuraProyUrl));
const bdmsEthAddress = process.env.BDMS_ETH_ADDRESS_FROM;
const bdmsEthprivKey = process.env.BDMS_ETH_ADDRESS_PRIVATE_KEY;
const ethSmartContractAddress = process.env.BDMS_ETH_ADDRESS_SMARTCONTRACT;

const dmsSmartContractManager = new web3.eth.Contract(abi, ethSmartContractAddress, {
  from: bdmsEthAddress,
  gas: process.env.BDMS_ETH_DEFAULT_GAS_FEE
});

helpers.isAuthenticated = (req, res, next) => {
  console.log('is authenticated??')
  console.log(req.user)

  if(req.user) {
    return next();
  } 
  req.flash('error_msg', 'No autorizado');
  res.redirect('/users/signin');
};

helpers.hasRole = (req, res, next) => {
  console.log('has role??')
  console.log(req.user.ethAddress)
  dmsSmartContractManager.methods.hasRole(req.user.ethAddress, req.user.role).call(function(error, result) {
    console.log('Smart contract validation role: ',result);
    if(result) {
      return next();
    }
    console.log(error);
    req.flash('error_msg', 'No posee role asignado');
    res.redirect('/');
  });
};

helpers.createAddress = (req, res) => {
  console.log('has address??');
  console.log(req);
    var account = web3.eth.accounts.create();
    console.log(account);
    //req.user.ethAddress = account.address;
  
  return account;
};

helpers.addUser = (user) => {
  console.log('Adding User to Smart contract!!!!');
  const role = user.role.code;
  const privateKey = user.privateKey;
  console.log(role);
  console.log(privateKey);

  // the address that will send the test transaction
  const addressFrom = bdmsEthAddress;

  // the destination address
  const addressTo = ethSmartContractAddress;

  const clientAccount = web3.eth.accounts.privateKeyToAccount(privateKey);
  const appAccount = web3.eth.accounts.privateKeyToAccount(bdmsEthprivKey);

  // set default address
  web3.eth.accounts.wallet.add(appAccount);
  web3.eth.defaultAccount = appAccount.address;


  // smart contract call
  const data = dmsSmartContractManager.methods.addUser(clientAccount.address, role, user.name).encodeABI();

  console.log('building transaction!!!!');

  // get the number of transactions sent so far so we can create a fresh nonce
  web3.eth.getTransactionCount(addressFrom).then(txCount => {

    // construct the transaction data
    const txData = {
      nonce: web3.utils.toHex(txCount),
      gasLimit: web3.utils.toHex(1000000),
      gasPrice: web3.utils.toHex(10e9), // 10 Gwei
      to: addressTo,
      from: addressFrom,
      value: web3.utils.toHex(web3.utils.toWei("0", "wei")),
      data: data
    }

    // fire away!
    sendSigned(txData, function(err, result) {
      if (err) {
        return console.log('error', err);
      }
      console.log('sent', result);
      user.ethTransactionHash = result;
      user.save();
    })
  });
};

// Signs the given transaction data and sends it. Abstracts some of the details 
// of buffering and serializing the transaction for web3.
function sendSigned(txData, cb) {
  console.log('sendSigned transaction!!!!');

  const privateKey = new Buffer(bdmsEthprivKey, 'hex');
  const transaction = new Tx(txData, { chain: 'ropsten' });
  transaction.sign(privateKey);
  const serializedTx = transaction.serialize().toString('hex');
  web3.eth.sendSignedTransaction('0x' + serializedTx, cb);
}

module.exports = helpers;

