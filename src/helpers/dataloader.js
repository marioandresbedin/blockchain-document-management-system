const Web3 = require('web3');
const {Role} = require('../models/Role');
const {Permission} = require('../models/Permission');


const utils = {};

// Smart contract initialization
const abi = require('../security/smartcontract/abi.json');
const infuraProyUrl = process.env.INFURA_PROJECT_URL;
const web3 = new Web3(new Web3.providers.HttpProvider(infuraProyUrl));
const bdmsEthAddress = process.env.BDMS_ETH_ADDRESS_FROM;
const ethSmartContractAddress = process.env.BDMS_ETH_ADDRESS_SMARTCONTRACT;
const dmsSmartContract = new web3.eth.Contract(abi, ethSmartContractAddress, {
  from: bdmsEthAddress,
  gas: process.env.BDMS_ETH_DEFAULT_GAS_FEE
});

utils.createPermissions = (req, res) => {

    Permission.find().then( result => {
        console.log(result);
        if(!result.length === 0) {
            console.log('Permissions already loaded!');
            return {};
        }
    }); 

    console.log('Genereting permissions!!!');
    const viewPerm = new Permission();
    viewPerm.code = "view_document";
    viewPerm.title = "View Document";
    viewPerm.save();

    const uploadPerm = new Permission();
    uploadPerm.code = "upload_document";
    uploadPerm.title = "Upload Document";
    uploadPerm.save();

    const analyticsPerm = new Permission();
    analyticsPerm.code = "view_analytics";
    analyticsPerm.title = "View Analytics";
    analyticsPerm.save();

    const apiIntegrationPerm = new Permission();
    apiIntegrationPerm.code = "api_integration";
    apiIntegrationPerm.title = "API Integration";
    apiIntegrationPerm.save();

    console.log('Permissions loaded!!!');
    return {viewPerm, uploadPerm, analyticsPerm, apiIntegrationPerm};

};

utils.createRoles = (req, res) => {
    console.log('Genereting roles!!!');
    Role.find().then( result => {
        console.log(result);
        if(!result.length === 0) {
            console.log('Roles already loaded!');
            return {};
        }
    }); 
    const userRole = new Role();
    userRole.code = "user_role";
    userRole.title = "User";
    userRole.save();

    const adminRole = new Role();
    adminRole.code = "admin_role";
    adminRole.title = "Admin";
    adminRole.save();
    
    return {userRole, adminRole};
};

utils.createAdminUser = (req, res) => {
    console.log('has address??');
    console.log(req);
      var account = web3.eth.accounts.create();
      console.log(account);
      //req.user.ethAddress = account.address;
    
    return account;
  
};

module.exports = utils;

