const passport = require('passport');
const  LocalStrategy = require('passport-local');
const {User} = require('../models/User');

passport.use(
  new LocalStrategy({
      usernameField: 'email'
    },
    async (email, password, done) => {
      // Match Email's User
      const user = await User.findOne({ email: email });

      if (!user) {
        console.log('User not found!')
        return done(null, false, { message: "Not User found." });
      } else {
        // Match Password's User
        const match = await user.matchPassword(password);
        if (match) {
          console.log('Login in success!')
          return done(null, user);
        } else {
          console.log('Password do not match!')
          return done(null, false, { message: "Incorrect Password." });
        }
      }
    }
  )
);

passport.serializeUser((user, done) => {
  console.log('searlizing user: ' + user.id )
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user);
  });
});
