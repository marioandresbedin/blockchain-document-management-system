const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const { Schema } = mongoose;

const PermissionSchema = new Schema({
  code : String,
  title : String,
}); // this are mostly static data

PermissionSchema.pre('save', async function(next) {
    // logic to synchronize token with blockchain
    console.log('Pre save permission!!!');
    
    next();
  });

const permSchemaModel = mongoose.model('Permission', PermissionSchema);

module.exports = {
    Permission: permSchemaModel
};