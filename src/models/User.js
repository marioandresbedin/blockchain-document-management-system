const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const {Role} = require('../models/Role');
const { createAddress, addUser } = require('../helpers/auth');

const { Schema } = mongoose;


const UserSchema = new Schema(
  {
    name: { type: String, trim: true },
    email: { type: String, required: true, unique: true, trim: true },
    password: { type: String, required: true },
    date: { type: Date, default: Date.now },
    role: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Role"
    },
    ethAddress: { type: String, required: false },
    privateKey: { type: String, required: false },
    ethTransactionHash: { type: String, required: false },
    isAdmin: { type: Boolean, default: false }
  }
);

UserSchema.methods.encryptPassword = async (password) => {
  const salt = await bcrypt.genSalt(10);
  const hash = bcrypt.hash(password, salt);
  return hash;
};

UserSchema.methods.matchPassword = async function (mail, role) {
  // call web3 
  console.log(mail);
  return mail;
};

UserSchema.pre('save', async function(next) {
  // logic to synchronize token with blockchain
  if(!this.role) {
    console.log('Assigning role!!!');
    if(this.isAdmin) {
      const adminRole = await Role.findOne({code: "admin_role"});
      console.log(adminRole);
      this.role = adminRole;
      console.log(adminRole);
    } else {
      const userRole = await Role.findOne({code: "user_role"});
      console.log(userRole);
      this.role = userRole;
      console.log(userRole);
    }
    var account = createAddress();
    console.log(account);
    this.ethAddress = account.address;
    this.privateKey = account.privateKey;
    addUser(this);
    console.log('Credentials has been saved!!!')
    next();
  } else {
    console.log('user updated!!');
    console.log('role!', this.role.code);
  }
});


const userSchemaModel = mongoose.model('User', UserSchema);

module.exports = {
  User: userSchemaModel
};