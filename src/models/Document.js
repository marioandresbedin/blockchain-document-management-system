const mongoose = require('mongoose');
const { Schema } = mongoose;

const DocumentSchema = new Schema({
    title: {
        type: String,
        required: true,
      },
      description: {
        type: String,
        required: false,
      },
      date: {
        type: Date,
        default: Date.now,
      },
      user: { 
        type: String,
        required: true
      },
      filename: { 
        type: String,
        required: true
      },
      urlHash: { 
        type: String,
        required: true
      },
      publicUrl: { 
        type: String,
        required: true
      },
      size: {
        type: String,
        required: false
      },
      extension: {
        type: String,
        required: false
      }
      
});

DocumentSchema.pre('save', async function(next) {
  // logic to synchronize token with blockchain
  console.log("after save document, trigger event!!!!!!")
  next();
});

module.exports = mongoose.model('Document', DocumentSchema);