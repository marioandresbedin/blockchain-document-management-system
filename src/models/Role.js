const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const {Permission} = require('../models/Permission');


const { Schema } = mongoose;

const RoleSchema = new Schema({
    code : String,
    title : String,
    permission : Array
  });

  RoleSchema.pre('save', async function(next) {
    // logic to synchronize token with blockchain
    console.log('Assign permissions!!!');
   
    // admin role
    if(this.code == "admin_role") {
      console.log('Assign permissions for admin user!!!');
      const analyticsPerm = await Permission.findOne({code: "view_analytics"});
      console.log(analyticsPerm);
      const apiPerm = await Permission.findOne({code: "api_integration"});
      console.log(apiPerm);
      this.permission = { analyticsPerm, apiPerm };

    }

    // default role
    if(this.code == "user_role") {
      console.log('Assign permissions for default user!!!');
      const viewPerm = await Permission.findOne({code: "view_document"});
      console.log(viewPerm);
      const uploadPerm = await Permission.findOne({code: "upload_document"});
      console.log(uploadPerm);
      this.permission = { viewPerm, uploadPerm };
    }

    next();
  });

  const roleSchemaModel = mongoose.model('Role', RoleSchema);

  module.exports = {
    Role: roleSchemaModel
  };