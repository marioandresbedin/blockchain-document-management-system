const router = require('express').Router();
const moment = require('moment');  
const Document = require('../models/Document');
const { isAuthenticated, hasRole } = require('../helpers/auth');


router.get("/", (req,res) => {
    if (req.user) {
        res.redirect('/dashboard');
    }
    res.render('index');
});

router.get("/about", (req,res) => {
    res.render('about');
});

router.get("/dashboard", isAuthenticated, async (req,res) => {
    console.log('Loading documents!!');
    console.log(req.user);
    const lastweek = moment().add(-7, 'days');
    console.log('lastweek:', lastweek);
    const recentDocs = await Document.find({ 
        $and: [{ 
            user: req.user.id 
        }, {
            date: { $gte: lastweek}
        }]
    }).sort({ date: 'desc' }).lean();
    console.log('documents founded:', recentDocs);

    const arrCountDoc = [];
    var dateArray = getDates(moment(), (moment().add(-7, 'days')));
    console.log('datearray', dateArray);

    for(const val of dateArray) {
        console.log(val)
        var count = await Document.countDocuments({
            $and: [{ 
                user: req.user.id 
            }, {
                date: {
                    $gte: moment(val).startOf('day'), 
                    $lt: moment(val).endOf('day')
                }
            }]
        });
        console.log("Count :", count);
        arrCountDoc.push(count);
    }
    console.log('Arr count docs by date: ', arrCountDoc);

    res.render('dashboard', { recentDocs, arrCountDoc, dateArray});
});



function getDates(stopDate, startDate) {
    console.log('**************');
    console.log('startdate', startDate);
    console.log('stopdate', stopDate);
    const dateArray = [];
    var currentDate = startDate;
    while (currentDate < stopDate) {
        const newDate = currentDate.add(1, 'days');
        console.log('Adding date:', newDate);
        dateArray.push(moment(newDate));
    }
    return dateArray;
}

module.exports = router;
