const router = require('express').Router();
const {User} = require('../models/User');
const { isAuthenticated, hasRole } = require('../helpers/auth');
const passport = require('passport');
const Web3 = require('web3');
const abi = require('../security/smartcontract/abi.json');
const infuraProyUrl = process.env.INFURA_PROJECT_URL;
const bdmsEthAddress = process.env.BDMS_ETH_ADDRESS_FROM;
const ethSmartContractAddress = process.env.BDMS_ETH_ADDRESS_SMARTCONTRACT;


const web3 = new Web3(new Web3.providers.HttpProvider(infuraProyUrl));

// Test smart contract
router.get("/users/smartcontract", (req,res) => {
  req.logout();
  const dmsSmartContract = new web3.eth.Contract(abi, ethSmartContractAddress, {
    from: bdmsEthAddress, // default from address
    gas: 100000
  });
  console.log('methods:');
  console.log(dmsSmartContract.methods);
  dmsSmartContract.methods.totalRoles().call(function(error, result) {
    console.log(result);
    console.log(error);
  });
  req.flash("success_msg", "SMART CONTRACT CALLED OK.");
  res.redirect('/');
});

// Sign In
router.get("/users/signin", (req,res) => {
  res.render('users/signin');
});

router.post("/users/signin", passport.authenticate('local', {
  successRedirect: '/dashboard',
  failureRedirect: '/users/signin',
  failureFlash: true,
  session: true
}));


// Sign Up
router.get("/users/signup", (req,res) => {
  res.render('users/signup');
});

router.post("/users/signup", async (req,res) => {
  const { name, email, password, confirm_password} = req.body;
  const errors = [];

  if(!name) {
    errors.push({text: 'Please you must enter a Name!'});
  }

  if(!email) {
    errors.push({text: 'Please you must enter an email!'});
  }

  if(password != confirm_password) {
    errors.push({text: 'Please check passwords must match!'});
  }
  console.log(req.body);
  if(password.length < 4) {
    errors.push({text: 'Password must have more than 4 characters!'});
  }
  if(errors.length > 0) {
    res.render('users/signup', { errors, name, email, password, confirm_password});
  } else {
     User.findOne({email: email}).then( emailUser => {
      if(emailUser) {
        req.flash('error_msg','The email is already in use!');
        res.redirect('/users/signup');
      }
    });    
    const newUser = new User({name, email, password});
    newUser.password = await newUser.encryptPassword(password);
    await newUser.save();
    req.flash('success_msg','User has been registered succesfully!');
    res.redirect('/users/signin');
  }
});

// Log out
router.get("/users/logout", (req,res) => {
  req.logout();
  req.flash("success_msg", "You are logged out now.");
  res.redirect('/');
});

// Get All Documents
router.get("/users", isAuthenticated, hasRole, async (req, res) => {
  console.log('Loading documents!!');
  console.log(req.user);
  const users = await User.find({ ethTransactionHash: { $ne: null} }).sort({ date: 'desc' }).lean();
  console.log('users founded:', users);
  res.render('users/all-users', { users });
});

module.exports = router;
