const router = require('express').Router();
const {User} = require('../../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const verifyToken = require('../../helpers/verify-token');
const Web3 = require('web3');


// Smart contract initialization
const abi = require('../../security/smartcontract/abi.json');
const infuraProyUrl = process.env.INFURA_PROJECT_URL;
const web3 = new Web3(new Web3.providers.HttpProvider(infuraProyUrl));
const bdmsEthAddress = process.env.BDMS_ETH_ADDRESS_FROM;
const bdmsEthprivKey = process.env.BDMS_ETH_ADDRESS_PRIVATE_KEY;
const ethSmartContractAddress = process.env.BDMS_ETH_ADDRESS_SMARTCONTRACT;

const dmsSmartContractManager = new web3.eth.Contract(abi, ethSmartContractAddress, {
  from: bdmsEthAddress,
  gas: process.env.BDMS_ETH_DEFAULT_GAS_FEE
});

// validation
const Joi = require('@hapi/joi');

const schemaUserRole = Joi.object({
    email: Joi.string().min(6).max(255).required().email()
});

router.get('/hasrole', verifyToken, async (req, res) => {
    // validaciones
    const { error } = schemaUserRole.validate(req.body);
    if (error) return res.status(400).json({ error: error.details[0].message })

    const user = await User.findOne({ email: req.body.email });
    if (!user) return res.status(400).json({ error: 'Usuario no encontrado' });
    
    console.log('User found:', user);
    console.log("request body:", req.body);
    dmsSmartContractManager.methods.getRoleInfos(user.ethAddress).call(function(error, result) {
        console.log(result);
        if(error) {
          console.log(error);
          return res.status(400).json({ error: 'Usuario no encontrado' });
        }
        if(result) {
          return res.status(200).json({user});
        } else {
          return res.status(401).json({ error: "Usuario no tiene el rol especificado."});
        }
    });
});

router.get('/totalusers', verifyToken, async (req, res) => {
    
    const user = req.user;
    if (!user) return res.status(400).json({ error: 'Usuario no encontrado' });

    console.log('User found:', user);
    dmsSmartContractManager.methods.getNumberOfUsers().call(function(error, result) {
        console.log(result);
        if(result) {
          return res.status(200).json({result});
        }
        console.log(error);
        return res.status(400).json({ error: 'No se encontraron usuarios almacenados en el smart contract' });
    });
});

module.exports = router;
