// general libraries
const router = require('express').Router();
const {User} = require('../../models/User');
const Document = require('../../models/Document');

// security libraries
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const verifyToken = require('../../helpers/verify-token');

// document management libraries
const multer = require('multer');
const upload = multer();
const fleekStorage = require('@fleekhq/fleek-storage-js')
const apiKey = process.env.FILECOIN_FLEEK_APIKEY;
const apiSecret = process.env.FILECOIN_FLEEK_APISECRET;

// validation
const Joi = require('@hapi/joi');

const schemaGetDocuments = Joi.object({
    email: Joi.string().min(6).max(255).required().email()
});

const schemaUploadDocument = Joi.object({
    title: Joi.string().min(1).max(255).required(),
    description: Joi.string().min(1).max(255).required()
});

router.get('/list', verifyToken, async (req, res) => {
    // validaciones
    console.log('body:', req.body);
    console.log('user: ', req.user);
    const { error } = schemaGetDocuments.validate(req.body);
    if (error) return res.status(400).json({ error: error.details[0].message })
    try {
        // Buscar documentos por usuario
        console.log('Usuer id:', req.user.id);
        let documents = await Document.find({ user: req.user.id }).sort({ date: 'desc' }).lean();
        console.log('documents founded:', documents);
        res.status(200).json({documents});
    } catch (err) {
        console.log('error:', err);
        return res.status(400).json({ error: err });
    }
});

router.post('/upload', verifyToken, upload.any(), async (req, res) => {

    console.log('body:', req.body.data);
    console.log('files:', req.files);
    const jsonData = JSON.parse(req.body.data);
    // validaciones
    const { error } = schemaUploadDocument.validate(jsonData);
    console.log('error', error);
    if (error) return res.status(400).json({ error: error.details[0].message })

    const { headers, files } = req;
    const { buffer, originalname: filename, size: sizeFile } = files[0];
    console.log(headers);
    console.log(files);
    const { title, description } = jsonData;
    
    try {
        console.log('****** Post to IPFS / Filecoin ******');
        const uploadedFile = await fleekStorage.upload({
            apiKey: apiKey,
            apiSecret: apiSecret,
            key: filename,
            data: buffer
        });
        console.log('****** Response from IPFS ******');
        console.log(uploadedFile);
        console.log('Saving document!!');
        const newDoc = new Document({ title, description });
        newDoc.user = req.user.id;
        newDoc.filename = filename;
        newDoc.extension = uploadedFile.extension;
        newDoc.urlHash = uploadedFile.hash;
        console.log(sizeFile);
        newDoc.size = formatBytes(sizeFile);
        newDoc.publicUrl = uploadedFile.publicUrl;
        await newDoc.save();
        console.log(newDoc);
        res.status(200).json({newDoc});
    } catch (err) {
        console.log('error:', err);
        return res.status(400).json({ error: err });
    }
    
});

router.get('/report/documents', verifyToken, async (req, res) => {
    // validaciones
    console.log('body:', req.body);
    console.log('user: ', req.user);
    const { error } = schemaGetDocuments.validate(req.body);
    if (error) return res.status(400).json({ error: error.details[0].message })
    try {
        // Buscar documentos por usuario
        console.log('Usuer id:', req.user.id);
        let documents = await Document.find({ user: req.user.id }).sort({ date: 'desc' }).lean();
        console.log('documents founded:', documents);
        res.status(200).json({documents});
    } catch (err) {
        console.log('error:', err);
        return res.status(400).json({ error: err });
    }
});

router.get('/report/users', verifyToken, async (req, res) => {
    // validaciones
    console.log('body:', req.body);
    console.log('user: ', req.user);
    const { error } = schemaGetDocuments.validate(req.body);
    if (error) return res.status(400).json({ error: error.details[0].message })
    try {
        // Buscar documentos por usuario
        console.log('Usuer id:', req.user.id);
        let documents = await Document.find({ user: req.user.id }).sort({ date: 'desc' }).lean();
        console.log('documents founded:', documents);
        res.status(200).json({documents});
    } catch (err) {
        console.log('error:', err);
        return res.status(400).json({ error: err });
    }
});

function formatBytes(a, b = 2, k = 1024) {
    with (Math) {
      let d = floor(log(a) / log(k));
      return 0 == a ? "0 Bytes" : parseFloat((a / pow(k, d)).toFixed(max(0, b))) + " " + ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"][d]
    }
  }


module.exports = router;
