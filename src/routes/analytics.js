const router = require('express').Router();
const User = require('../models/User');
const passport = require('passport');
const  {isAuthenticated}  = require('../helpers/auth');



// See data analitycs
router.get("/data-analytics", isAuthenticated, (req,res) => {
    console.log('loading data analytics page')
    res.render('analytics/data-reports');
});



module.exports = router;

