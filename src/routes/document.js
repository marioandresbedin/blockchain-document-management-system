const router = require('express').Router();
const Document = require('../models/Document');
const { isAuthenticated, hasRole } = require('../helpers/auth');
const multer = require('multer');
const upload = multer();
const fleekStorage = require('@fleekhq/fleek-storage-js')

const apiKey = process.env.FILECOIN_FLEEK_APIKEY;
const apiSecret = process.env.FILECOIN_FLEEK_APISECRET;

// New document
router.get("/documents/add", (req, res) => {
  res.render('documents/new-document');
});

router.post("/documents/new-document", isAuthenticated, upload.any(), async (req, res) => {
  console.log('****** Uploading document! ******');
  const { headers, files } = req;
  const { buffer, originalname: filename, size: sizeFile } = files[0];
  console.log(headers);
  console.log(files);
  const { title, description } = req.body;
  const errors = [];
  if (!title) {
    errors.push({ text: 'Please insert a title!' });
  }
  if (errors.length > 0) {
    res.render('documents/new-document', {
      errors,
      title,
      description
    });
  } else {
    console.log('****** Post to IPFS / Filecoin ******');
    const uploadedFile = await fleekStorage.upload({
      apiKey: apiKey,
      apiSecret: apiSecret,
      key: filename,
      data: buffer,
    });
    console.log('****** Response from IPFS ******');
    console.log(uploadedFile);
    console.log('Saving document!!');
    const newDoc = new Document({ title, description });
    newDoc.user = req.user.id;
    newDoc.filename = filename;
    newDoc.extension = uploadedFile.extension;
    newDoc.urlHash = uploadedFile.hash;
    console.log(sizeFile);
    newDoc.size = formatBytes(sizeFile);
    newDoc.publicUrl = uploadedFile.publicUrl;
    await newDoc.save();
    console.log(newDoc);
    req.flash('success_msg', 'Documento almacenado correctamente!')
    res.redirect('/documents');
  }
});

// Get All Documents
router.get("/documents", isAuthenticated, hasRole, async (req, res) => {
  console.log('Loading documents!!');
  console.log(req.user);
  const documents = await Document.find({ user: req.user.id }).sort({ date: 'desc' }).lean();
  console.log('documents founded:');
  //console.log(documents);
  res.render('documents/all-documents', { documents });
});


// Edit Document
router.get("/documents/edit/:id", isAuthenticated, hasRole, async (req, res) => {
  const doc = await Document.findById(req.params.id).lean();
  console.log(doc);
  res.render('documents/edit-document', { doc });
});

router.put("/documents/edit-document/:id", isAuthenticated, async (req, res) => {
  const { title, description } = req.body;
  await Document.findByIdAndUpdate(req.params.id, { title, description });
  req.flash('success_msg', 'Document updated successfully!')
  res.redirect('/documents');
});


// delete Document
router.delete("/documents/delete/:id", isAuthenticated, async (req, res) => {
  await Document.findByIdAndDelete(req.params.id).lean();
  req.flash('success_msg', 'Documento eliminado correctamente!')
  res.redirect('/documents');
});


// load sample data
// Get All Documents
router.get("/sample-data", isAuthenticated, async (req, res) => {
  console.log('Loading sample data!!');
  console.log(req.user);

  // see file information from ipfs
  const files = await fleekStorage.listFiles({
    apiKey: apiKey,
    apiSecret: apiSecret,
    getOptions: [
      'bucket',
      'key',
      'hash',
      'publicUrl'
    ],
  });
  for (let item of files) {
    const newDoc = new Document({});
    newDoc.user = req.user.id;
    newDoc.filename = item.key;
    newDoc.title = 'Loaded from sample data!';
    newDoc.urlHash = item.hash;
    newDoc.publicUrl = item.publicUrl;
    await newDoc.save();
  }
  console.log('sample data has loaded!!');
  res.redirect('/documents');
});

function formatBytes(a, b = 2, k = 1024) {
  with (Math) {
    let d = floor(log(a) / log(k));
    return 0 == a ? "0 Bytes" : parseFloat((a / pow(k, d)).toFixed(max(0, b))) + " " + ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"][d]
  }
}


module.exports = router;
