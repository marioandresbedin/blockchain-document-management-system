const router = require('express').Router();
const { createRoles, createPermissions } = require('../helpers/dataloader');
const { addUser } = require('../helpers/auth');
const {User} = require('../models/User');



router.get("/sampledata", (req,res) => {

    var permissions = createPermissions();
    console.log("Permissions created:");
    console.log(permissions);

    var roles = createRoles();
    console.log("Roles created:");
    console.log(roles);

    req.flash('success_msg', 'Sample Data loaded successfully!')
    res.redirect('/documents');
});

router.get("/testcontract", (req,res) => {
    var resultRole = '';
    var restulPrivateKey = '';
    User.findOne({email: "elon@gmail.com"}).then( emailUser => {
        if(emailUser) {
          console.log(emailUser);
          resultRole = emailUser.role;
          restulPrivateKey = emailUser.privateKey;
        }
    }); 
    console.log(resultRole);
    console.log(restulPrivateKey);
    
    const resultEth = addUser(resultRole, restulPrivateKey);
    console.log(resultEth);
    console.log('Credentials has been saved!!!')

});

module.exports = router;
