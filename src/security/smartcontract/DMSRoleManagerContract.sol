// SPDX-License-Identifier: TFG - Mario Bedin
pragma solidity ^0.8.0;
/**
* @title RBAC for BDMS system
* @author Mario Bedin
* @notice Implements runtime configurable Role Based Access Control.
*/
contract DMSRoleManagerContract {

    bool public status;
    address public admin;
    string public organizationName;
    uint public numberOfUsers;
    mapping (address => uint) public userId;
    mapping (address => User) public usersStored;

    User[99] public users;
    
    event UserAdded(address UserAddress, string UserRole);
    event UserUpdated(address UserAddress, string UserRole);
    event UserRemoved(address UserAddress);
    event StatusChanged(string Status);
    
    struct User {
        address user;
        string role;
        string userName;
        uint userSince;
    }
    
    modifier onlyAdmin {
        require(msg.sender == admin);
        _;
    }
    
    modifier onlyUsers {
        require((userId[msg.sender] != 0) || msg.sender == admin);
        _;
    }
    
    constructor (string memory enterOrganizationName) {
        admin = msg.sender;
        status = true;
        numberOfUsers = 0;
        addUser(admin, 'Creator and Admin of Smart Contract', 'admin');
        organizationName = enterOrganizationName;
    }
    
    function addUser(address userAddress, string memory userRole, string memory username) onlyAdmin public {
        require(status = true);
        uint id = userId[userAddress];
        if (id == 0) {
            userId[userAddress] = numberOfUsers;
            id = numberOfUsers;
            users[id] = User({user: userAddress, userSince: block.timestamp, role: userRole, userName: username});
            emit UserAdded(userAddress, userRole);
            numberOfUsers++;
        }
        else {
            users[id] = User({user: userAddress, userSince: block.timestamp, role: userRole, userName: username});
            emit UserUpdated(userAddress, userRole);
        }
    }
    
    function removeUser(address userAddress) onlyAdmin public {
        require(status = true);
        require(userId[userAddress] != 0);
        address ad;
        for (uint i = userId[userAddress]; i<numberOfUsers-1; i++){
            users[i] = users[i+1];
            ad = users[i].user;
            userId[ad] = i;
        }
        delete users[numberOfUsers-1];
        emit UserRemoved(userAddress);
        numberOfUsers--;
        userId[userAddress] = 0;
    }
    
    
    function changeStatus (bool deactivate) onlyAdmin public {
        if (deactivate)
        {status = false;}
        emit StatusChanged("Smart Contract Deactivated");
    }
  
    function getNumberOfUsers() public view returns (uint256) {
        return numberOfUsers - 1;
    }

     /**
   * @notice Verify whether an account is a bearer of a role
   * @param userAddress The address to verify.
   * @param userRole The role to look into.
   * @return Whether the account is a bearer of the role.
   */
  function hasRole(address userAddress, string memory userRole) public view returns(bool) {
    User memory user = usersStored[userAddress];
    string memory roleUsr = user.role;
    return compareStringsbyBytes(roleUsr, userRole);
  }

  function compareStringsbyBytes(string memory s1, string memory s2) public pure returns(bool){
    return (keccak256(bytes(s1)) == keccak256(bytes(s2)));
  }

   /**
   * @notice Get role information for a given account 
   * @param userAddress The address to verify.
   * @return Whether the account is a bearer of the role.
   */
  function getRoleInfos(address userAddress) public view returns (string memory) {
    return (
            usersStored[userAddress].role
    );
  }



}