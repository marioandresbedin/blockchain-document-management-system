$(function () {

    var colors = {
            primary: $('.colors .bg-primary').css('background-color').replace('rgb', '').replace(')', '').replace('(', '').split(','),
            secondary: $('.colors .bg-secondary').css('background-color').replace('rgb', '').replace(')', '').replace('(', '').split(','),
            info: $('.colors .bg-info').css('background-color').replace('rgb', '').replace(')', '').replace('(', '').split(','),
            success: $('.colors .bg-success').css('background-color').replace('rgb', '').replace(')', '').replace('(', '').split(','),
            danger: $('.colors .bg-danger').css('background-color').replace('rgb', '').replace(')', '').replace('(', '').split(','),
            warning: $('.colors .bg-warning').css('background-color').replace('rgb', '').replace(')', '').replace('(', '').split(','),
        },
        chartFontStyle = 'Josefin Sans';

    var rgbToHex = function (rgb) {
        var hex = Number(rgb).toString(16);
        if (hex.length < 2) {
            hex = "0" + hex;
        }
        return hex;
    };

    var fullColorHex = function (r, g, b) {
        var red = rgbToHex(r);
        var green = rgbToHex(g);
        var blue = rgbToHex(b);
        return red + green + blue;
    };

    colors.primary = '#' + fullColorHex(colors.primary[0], colors.primary[1], colors.primary[2]);
    colors.secondary = '#' + fullColorHex(colors.secondary[0], colors.secondary[1], colors.secondary[2]);
    colors.info = '#' + fullColorHex(colors.info[0], colors.info[1], colors.info[2]);
    colors.success = '#' + fullColorHex(colors.success[0], colors.success[1], colors.success[2]);
    colors.danger = '#' + fullColorHex(colors.danger[0], colors.danger[1], colors.danger[2]);
    colors.warning = '#' + fullColorHex(colors.warning[0], colors.warning[1], colors.warning[2]);
    
    function dailyUsage() {
        const now = new Date();
        const length = 7;

        const days = Array.from({ length }, (_, days) => {
            let current = new Date(now); // clone "now"
            current.setDate(now.getDate() - days); // change the date
            let day = current.getDate();
            let month = current.getMonth() + 1;
            let year = current.getFullYear();
            if(month < 10){
                return `${day}-0${month}-${year}`;
            } else {
                return `${day}-${month}-${year}`;
            }
        })

        console.log(days)
        var strCountDoc = $("#recent-dates").val();
        var arrCountData = strCountDoc.split(',');
        console.log(arrCountData);


        var options = {
            series: [{
                name: 'Files',
                data: arrCountData
            }],
            chart: {
                type: 'bar',
                fontFamily: chartFontStyle,
                offsetX: -18,
                height: 390,
                width: '103%',
                toolbar: {
                    show: false
                }
            },
            colors: [colors.primary, colors.success],
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 10,
                colors: ['transparent']
            },
            xaxis: {
                categories: days.reverse(),
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val
                    }
                }
            },
            legend: {
                show: false
            }
        };

        var chart = new ApexCharts(document.querySelector("#daily-usage"), options);

        chart.render();
    }
    console.log('************ testing javascript objects ************************');
    dailyUsage();
});
