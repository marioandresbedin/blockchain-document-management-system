const express = require('express');
const path = require('path');
const Handlebars = require('handlebars');
const exphbs = require('express-handlebars');
const { allowInsecurePrototypeAccess } = require('@handlebars/allow-prototype-access');

const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('connect-flash');
const passport = require('passport');
const moment= require('moment');
require('dotenv').config({path: path.resolve(__dirname+'/config.env')});

// API rest changes
const bodyparser = require('body-parser');



const morgan = require('morgan');
const MongoStore = require('connect-mongo');
const cors = require('cors')



// Initializations
const app = express();
require('./database');
require('./config/passport');

// settings
app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "views"));
app.engine(
  ".hbs",
  exphbs({
    helpers: {
      formatDate : function(dateString) {
        return new Handlebars.SafeString(
            moment(dateString).format("DD/MM/YYYY").toUpperCase()
        );
      },
      ifEquals : function(arg1, arg2, options) {
        return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
      }
    },
    defaultLayout: "main",
    layoutsDir: path.join(app.get("views"), "layouts"),
    partialsDir: path.join(app.get("views"), "partials"),
    extname: ".hbs",
    handlebars: allowInsecurePrototypeAccess(Handlebars)
  })
);
app.set("view engine", ".hbs");

// middlewares
app.use(express.urlencoded({extended: false}));
app.use(methodOverride('_method'));
app.use(session({
  secret: 'mysecretapp',
  resave: true,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(cors());
// capturar body
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());



// Global Variables
app.use((req, res, next) => {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  res.locals.user = req.user || null;
  next();
});

// routes
app.use(require('./routes/index'));
app.use(require('./routes/users'));
app.use(require('./routes/document'));
app.use(require('./routes/analytics'));
app.use(require('./routes/sampledata'));
app.use(require('./routes/api/user'));
app.use(require('./routes/api/document'));

// api routes
app.use('/api/users', require('./routes/api/user'));
app.use('/api/documents', require('./routes/api/document'));
app.use('/api/security', require('./routes/api/security'));



// static files
app.use(express.static(path.join(__dirname, "public")));
app.use((req, res) => {
  res.render("404");
});

// Listening
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});