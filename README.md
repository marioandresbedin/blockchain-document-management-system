# Blockchain Document Mangement System

This is a Prototype App to manage documents by exploiting blockchain capabilities using Nodejs, Mongodb, and other related technologies.

This prototype app can do:

- CRUD Operations: create/read/update/delete Documents
- Allows a user to regiter, do login and upload his personal documents


# UAT and Production Environment Variables (NOT READY)

This app needs the following environment Variables

- `MONGODB_HOST` this is the Mongodb URI string
- `MONGODB_DATABASE` Mongodb database name
- `NODE_ENV` node environment

# Docker (NOT READY)

you can run a container for development

```shell
npm install
docker-compose up
```

## Default User

when the app is lauched, this will create an Admin user with the following credentials:

- email: admin@localhost
- password: admin

# Resources

- [Materia Bootswatch](https://www.bootstrapcdn.com/bootswatch/)
- [CSS Background uiGradients](https://uigradients.com/#Dull)

